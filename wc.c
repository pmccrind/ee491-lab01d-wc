/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc.c
/// @version 1.4 - count chars, and count lines works with unicode
///
/// wc - print newline, word, and byte counts for each file
///
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    25_Jan_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////

//@TODO review all includes to ensure they are required
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include "wc.h"

//Need Argc and argv
//@argc - argument count passed to the program, expected 1 or more
//@argv - argument list passed to the program, expected 
//["./wc", "somefile.txt", "-c"] as well as additional files and 
//flags. Also accepted is no files and flags
int main( int argc, char *argv[] ) {
   
   //Commandline Parsing will be completed here

   //three single flags: -c, -l, OR -w
   //four double flags: --bytes, --lines, --words, OR 
   //--version

   //No File Provided read from stdin
   //No flags either provide standard output
   if (argc == 1) {

      //store file from standardIn function
      FILE* fileptr = readFromStandardIn();
      
      //Make sure file pointer is not null
      if (fileptr == NULL){
         exit(EXIT_FAILURE);
      }
     
      //Place the file ptr at the start of the file
      fseek(fileptr, 0, SEEK_SET);
      
      //Count the number of lines
      int numOfLines = countLines(fileptr);
      //count the number of characters
      int numOfChars = countChars(fileptr);
      //Count the number of words
      int numOfWords = countWords(fileptr);
      
      //expected output
      printf("%d \t %d \t %d \t \n", numOfLines, numOfWords, numOfChars);

      //Close the file
      int fileClosed = fclose(fileptr);

      //Check the file closed correctly
      if(fileClosed != 0){
         exit(EXIT_FAILURE);
      }

      //expectd output

      exit(EXIT_SUCCESS);
 
   } else {
      //Two or more arguments recieved, need to find all valid flag commandline
      //arguemnts. Then open files. If no files then the program should read
      //from standard in.
      
      
      //flag for char output
      bool bytes = false;
      //flag for word output
      bool words = false;
      //flag for line output
      bool lines = false;

 
      //flag will be set if an attempt to open a file
      //is tired if still false then read from standard
      //in.
      bool opened = false;
      
      //loop through the given arguments looking for 
      //the single and double flags, setting them to true
      //as they are found.
      //Set the opened flag to true if no matches are made any of the
      //conditional checks
      for ( int i = 1; i < argc; i ++ ){
         
         //look double flag characters
         
         //single flag character
         if (argv[i][0] == '-'){
            
            //Check for second flag character
            if(argv[i][1] == '-'){
      
               //compare to see which double flag it is 
               //error if none
               //Character output
               if (strcmp(argv[i], "--bytes") == 0){
                  bytes = true;
                  
                  //on a match no need to keep checking
                  //continue on to the next check
                  continue;


               //Line Output
               } else if ( strcmp(argv[i], "--lines") == 0) {
                  lines = true;
                  
                  //on a match no need to keep checking
                  //continue on to the next check
                  continue;
               
               //Word Output
               } else if (strcmp(argv[i], "--words") == 0) {
                  words = true;
                  
                  //on a match no need to keep checking
                  //continue on to the next check 
                  continue;
               
               //Verison requested
               } else if (strcmp(argv[i], "--version") == 0){
                  
                  //When version is requested all other flags and 
                  //files are ignored. Thus if found display and 
                  //close.
                  printf("%s", VERSION);
                  exit(EXIT_SUCCESS);
               } else {
                  //No matches Error and Close
                  printf("%s: unrecognized option %s \n", FILENAME, argv[i]); 
                  exit(EXIT_FAILURE); 

               }

            }//end double flag if
            
            //not a second flag char

            //Character Output
            if(strcmp(argv[i], "-c") == 0){
               bytes = true;
               continue;

            //Word Output
            } else if (strcmp(argv[i], "-w") == 0) {
               words = true;
               continue;

            //Line Outpu
            } else if (strcmp(argv[i], "-l") == 0){
               lines = true;
               continue;
            //no match error and close
            } else {
               printf("%s: unrecognized option %s \n", FILENAME, argv[i]); 
               exit(EXIT_FAILURE); 
            }

         }//end double flag if

         //not a flag thus their is a file to be opened

         opened = true;
      
      }//end argument for loop
      
      
      //Flags have now been set but if opened is false
      //then there where no files, thus read from
      //standard in.
      if ( !opened ){
         //read from stdin
         
         //store file pointer returned from stanard input
         //function
         FILE* fileptr = readFromStandardIn();
      
         //Make sure file pointer is not null
         if (fileptr == NULL){
            exit(EXIT_FAILURE);
         }
      
         
     
         //Place the file ptr at the start of the file
         fseek(fileptr, 0, SEEK_SET);
      
         //Count the number of lines
         int numOfLines = countLines(fileptr);
         //count the number of characters
         int numOfChars = countChars(fileptr);
         //Count the number of words
         int numOfWords = countWords(fileptr);
      
         //need 4 flags, file name and 3 values 
         printOutput(lines, words, bytes, false, "", numOfLines, numOfWords, numOfChars);          

         //Close the file
         int fileClosed = fclose(fileptr);

         //Check the file closed correctly
         if(fileClosed != 0){
            exit(EXIT_FAILURE);
         }

         //expectd output

         exit(EXIT_SUCCESS);

      }//end !opened check
      
      //If we get past the check their must be a file
      
     

      //file pointer
      FILE* fileptr = NULL;
      
      //file name
      char* file;
      
      //Count the Number of Chars
      int numChars = 0;

      //Count number of \n in the File
      int numLines = 0; 
      
      //Count number of words
      int numWords = 0;
      //stores total number of chars for 2 more file
      int totalChars = 0;
      //stores total number of lines for 2 more files
      int totalLines = 0;
      //stores total number of words for 2 or more files
      int totalWords = 0;
      //Used to see if more than 2 files were opened
      int numOfFiles = 0;
      
      //used to see if the file close successfully
      int fileClosed = 0;
            

      //loop through the command line arguments attempting to open
      //all non-flags
      for ( int j = 1; j < argc; j++ ){

         //check for non-flags
         if(argv[j][0] != '-' ){
            //found a file
            numOfFiles++; 
            //File Name
            file =  argv[j];
      
            //Open the given file
            fileptr = fopen(file, "r");

            //Check if file opened correctly
            if (fileptr == NULL){
               //File could not be opened.
               printf("%s: Can't open [%s]\n", FILENAME, file);
               exit(EXIT_FAILURE);

            } 

            //File Opened Successfully

            //Count Lines, Words, and Chars
         
            //Count the Number of Chars
            numChars = countChars(fileptr);

            //add to char running total
            totalChars += numChars;

            //Count number of \n in the File
            numLines = countLines(fileptr); 
      
            //add to line running total
            totalLines += numLines;

            //Count number of words
            numWords = countWords(fileptr);
      
            //add to word running total
            totalWords += numWords;
            

            //need 4 flags, file name and 3 values 
            printOutput(lines, words, bytes, false, file, numLines, numWords, numChars);           

            //Close File
            fileClosed = fclose(fileptr);   
         
            if( fileClosed != 0 ){
               //File did not close Successfully
               exit(EXIT_FAILURE);

            } 
      
            //File Closed Sucessfully, keep reading the rest of the arguments.
            
            //Set pointer back to NULL in case their is another file
            fileptr = NULL;
             

         } else {
            //its a flag do nothing
         
         }


      } //end file opening loop

      
      //If the number of files opened by the above loop is greater than
      //or equal to 2 than a total should be printed
      if ( numOfFiles >= 2 ){
         //need 4 flags, file name and 3 values 
         printOutput(lines, words, bytes, true, file, totalLines, totalWords, totalChars);           

       
      }//end numOfiles flag
      
      //all flags parsed, all files opened and all printouts compelte
      exit(EXIT_SUCCESS);

   }//end argc >= 2 case
      
   //Should not reach this section,
   //exit with failure if you do.
   exit(EXIT_FAILURE);

}//End Main Function
 

