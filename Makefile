###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01d - wc - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.1 - added in all test file comparing against builtin wc
###
### Build a Word Counting program
###
### @author  Patrick McCrindle <pmccrind@hawaii.edu>
### @date    19_Jan_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = wc

all: $(TARGET)

wc: wc.c
	$(CC) $(CFLAGS) wc_functions.c  -o $(TARGET) wc.c

test: wc
	./wc /users8/students2/pmccrind/EE491F/test_files/test0 
	./wc /users8/students2/pmccrind/EE491F/test_files/test1
	./wc /users8/students2/pmccrind/EE491F/test_files/test2
	./wc /users8/students2/pmccrind/EE491F/test_files/test3
	./wc /users8/students2/pmccrind/EE491F/test_files/test4
	./wc /users8/students2/pmccrind/EE491F/test_files/test5
	./wc /users8/students2/pmccrind/EE491F/test_files/test8
	./wc /users8/students2/pmccrind/EE491F/test_files/test9
	./wc /users8/students2/pmccrind/EE491F/test_files/testa
	/usr/bin/wc /users8/students2/pmccrind/EE491F/test_files/test0
	/usr/bin/wc /users8/students2/pmccrind/EE491F/test_files/test1
	/usr/bin/wc /users8/students2/pmccrind/EE491F/test_files/test2
	/usr/bin/wc /users8/students2/pmccrind/EE491F/test_files/test3
	/usr/bin/wc /users8/students2/pmccrind/EE491F/test_files/test4
	/usr/bin/wc /users8/students2/pmccrind/EE491F/test_files/test5
	/usr/bin/wc /users8/students2/pmccrind/EE491F/test_files/test8
	/usr/bin/wc /users8/students2/pmccrind/EE491F/test_files/test9
	/usr/bin/wc /users8/students2/pmccrind/EE491F/test_files/testa

clean:
	rm -f $(TARGET)

