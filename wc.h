/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc.h
/// @version 1.0 - includes function declartions in wc_fns
///
/// wc - print newline, word, and byte counts for each file
///
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    25_Jan_2022
///
///////////////////////////////////////////////////////////////////////////////

#define VERSION "\n wc 1.4 \n Copy Right (C) Free Software Foundation, Inc.\n"
#define FILENAME "wc"


//count Chars function
int countChars(FILE *filePointer);
//count Lines  function
int countLines(FILE *filePointer);
//count words function
int countWords(FILE *filePointer);
//read from standard input and save to a file
FILE* readFromStandardIn();
//print with the desired output
//lines for line output, words for word output, chars for char output
//total for more than one file to print out total, filename for files,
//num of lines for line count, num of words for word count, and 
//num of chars for character count
void printOutput(bool lines, bool words, 
      bool bytes, bool total, char* filename,
      int numOfLines, int numOfWords, int numOfChars);
