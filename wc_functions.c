/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc_functions.c
/// @version 1.0 - contains needed functions for wc
///
/// wc - print newline, word, and byte counts for each file
///
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    25_Jan_2022
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

//Counts the number of characters in the 
//given file.
//Works with all given test files.
int countChars(FILE *incomingFile){

   //Check the incoming pointer is not 
   //null.
   if (incomingFile == NULL){
      exit(EXIT_FAILURE);

   }

   //Unsigned to support larger files.
   unsigned int numOfChars = 0;
   

   //Go to the End of File regardless of where
   //we are in the file, with no offset
   fseek(incomingFile, 0, SEEK_END);
   //Get Current Postion from beginning of File
   numOfChars = ftell(incomingFile);
   //numOfChars now has number of chars
   //Setting file pointer back to Start
   fseek(incomingFile, 0, SEEK_SET);

   return numOfChars;


}//End Count Chars

//Counts number of  Lines in the given file
//works with all given test files.
int countLines(FILE *incomingFile){
   //Check that incoming pointer is not
   //null.
   if (incomingFile == NULL){
      exit(EXIT_FAILURE);

   }
   

   //Make sure the filepointer is at the
   //beginning of the file.
   //fseek(incomingFile, 0, SEEK_SET);

   //File Pointer to the line.
   char *readLine;
   //buffer Size needed for the line
   size_t len = 0;
   //Size of line returned from getline()
   ssize_t read;

   //total number of '\n' terminated lines
   int numOfLines = 0;
   
   //Read in File line by line
   //@TODO define the -1 return value from getline()
   while( (read = getline(&readLine, &len, incomingFile))  != -1 ) {
         
      //Only increase count if line is terminated by
      //'\n'. Thus can not just return count of lines
      //as it would include EOF. Which may not be
      //terminated in '\n'.
      if(readLine[read - 1] == 10){
         numOfLines++;
      }

   }
  
   
   //Setting file pointer back to Start of File
   fseek(incomingFile, 0, SEEK_SET);

   return numOfLines;

}//End Count Lines


//Count number of Words in the given File
//Does not work for Tests: test0 and test4.
//isspace does not work with unicode
//characters.
//@TODO add in support for unicode
int countWords(FILE *incomingFile){
   
   //Check that incoming pointer is not
   //null.
   if (incomingFile == NULL){
      exit(EXIT_FAILURE);

   }

   //Make sure file pointer is at start of
   //file.
   fseek(incomingFile, 0, SEEK_SET);
   
   //Count of the number of words
   int numOfWords = 0;
   
   //Used to read in the file char by char
   char c;

   //Used as a flag to check if we are in
   //a word or not.
   bool inAWord = false;
   
   
   //Read in File Char by Char until end of file
   while( (c = fgetc(incomingFile)) != EOF ){ 

      //c is a space
      if(isspace(c)){
         
         //keep reading white space
         while((c = fgetc(incomingFile)) != EOF ){

            //see another space do nothing
            if (isspace(c)){
               //Do nothing

            //see a char and break and switch
            } else {
               //if we see a valid character set the
               //flag.
               inAWord = true;
               //increase count
               numOfWords++;
               //break into the outer while loop
               break;
            }


         }//end read white space while


      //c is a character
      } else {
         
         //If we are not in word before we are now.
         if(!inAWord){
            //increase word count
            numOfWords++;
            //@TODO Should this be setting the word
            //to true? Need to double check
            inAWord = false;
         }

         //keep reading characters until a space 
         //is found
         while((c = fgetc(incomingFile)) != EOF ){
            
            //c is a space 
            if(isspace(c)){
               //no longer in a word
               inAWord = false;
               //break into outer while loop
               break;
               

            //c is a character
            } else {
               //Still in a word, do nothing.

            }
         

         }//end read chars while loop


      }//end outer else


   }//End Outer While Loop
   
   //Setting file pointer back to Start of File
   fseek(incomingFile, 0, SEEK_SET);
   
   return numOfWords;

}//End Count Words


//reads and accepts input from stdin
//store stdin into a temporary file
//which is deleted after the program has finished running
FILE* readFromStandardIn(){

   //create a temporary file to store the information
   //from stdin
   FILE* fileptr = tmpfile();
      
   //Make sure file pointer is not null
   if (fileptr == NULL){
      exit(EXIT_FAILURE);
   }
      
   //File Pointer to the line.
   char *readLine;
   
   //buffer Size needed for the line
   size_t len = 0;
   //Size of line returned from getline()
   ssize_t read;

   
   //Read in line by line from stdin storing into tmpfile
   while( (read = getline(&readLine, &len, stdin))  != -1 ) {
      //Put the standard input into the tmp file
      fputs(readLine, fileptr);
         
         
   }
     
   //Place the file ptr at the start of the file
   fseek(fileptr, 0, SEEK_SET);
      
   
   return fileptr;


}//end readFromStandardIn function


//Provide the expected output
void printOutput(bool lines, bool words, 
      bool bytes, bool total, char* filename,
      int numOfLines, int numOfWords, int numOfChars){
   
   //Print requested Output
   
   //if no total or file name print only numOfLines,
   //numOfWords, and numOfChars
   if ((!total) && (strcmp(filename, "") == 0)  ){
      
      //Print out based off of the given flags
      if ((bytes) && (words) && (lines)){
      
         //default output lines, words, and chars
         printf("%d \t %d \t %d \t \n", numOfLines, numOfWords, numOfChars);

      } else if ((bytes) && (words)) {
      
         //only bytes and words
         printf("%d \t %d \t \n", numOfWords, numOfChars);


      } else if ((bytes) && (lines)) {
      
         //only bytes and lines
         printf("%d \t %d \t \n", numOfLines, numOfChars);
            

      } else if ((words) && (lines)) {
      
         //only lines and words
         printf("%d \t %d \t \n", numOfLines, numOfWords);

      } else if (bytes) {
      
         //only chars
         printf("%d \t \n", numOfChars);


      } else if (words) {
      
         //only words
         printf("%d \t \n", numOfWords);


      } else if (lines) {
         //only lines
         printf("%d \t \n", numOfLines);

      } else {
         //No Flags Normal output
         printf("%d \t %d \t %d \t \n", numOfLines, numOfWords, numOfChars);
         
      }
      
      //Shouldn't reach this point error if you do
      //exit(EXIT_FAILURE);

   
   //end no total or file name if
   
   } else if (!total) {
      //print out with the file name
      
      //Print requested Output
      if ( (bytes) && (words) && (lines) ){
         //default output lines, words, and chars

         printf("%d \t %d \t %d \t %s \n", numOfLines, numOfWords, numOfChars, filename);

      } else if ( (bytes) && (words) ) {
         //only bytes and words

         printf("%d \t %d \t %s \n", numOfWords, numOfChars, filename);


      } else if ( (bytes) && (lines) ) {
         //only bytes and lines

         printf("%d \t %d \t %s \n", numOfLines, numOfChars, filename);
            

      } else if ( (words) && (lines) ) {
         //only lines and words
         printf("%d \t %d \t %s \n", numOfLines, numOfWords, filename);
            
      } else if ( bytes ) {
         //only chars
         printf("%d \t %s \n", numOfChars, filename);


      } else if ( words ) {
         //only words
         printf("%d \t %s \n", numOfWords, filename);


      } else if ( lines ) {
         //only lines
         printf("%d \t %s \n", numOfLines, filename);

      } else {
         //No Flags Normal output
         printf("%d \t %d \t %d \t %s \n", numOfLines, numOfWords, numOfChars, filename);
      }
   
      //end print out with the file name

   } else {
      //print out the total
      
      //Print requested Output
      if ( ( bytes ) && ( words ) && ( lines ) ){
         //default output lines, words, and chars
         printf("%d \t %d \t %d \t total \n", numOfLines, numOfWords, numOfChars);

      } else if ( ( bytes ) && ( words ) ) {
         //only bytes and words

         printf("%d \t %d \t total \n", numOfWords, numOfChars);


      } else if ( ( bytes ) && ( lines ) ) {
         //only bytes and lines
         printf("%d \t %d \t total \n", numOfLines, numOfChars);
            

      } else if ( ( words ) && ( lines ) ) {
         //only lines and words
         printf("%d \t %d \t total \n", numOfLines, numOfWords);
            
      } else if ( bytes ) {
         //only chars
         printf("%d \t total \n", numOfChars);

      } else if ( words ) {
         //only words
         printf("%d \t total\n", numOfWords);

      } else if ( lines ) {
         //only lines
         printf("%d \t total \n", numOfLines);

      } else {
         //No Flags Normal output
         printf("%d \t %d \t %d \t total \n", numOfLines, numOfWords, numOfChars);
      }



   }//end total print out
   
   


}//end print function


